/**
 * Created by abderrahimelimame on 9/24/16.
 */
module.exports = function (io, users, debugging_mode, path, hostname, os, mainPort) {


    io.on('connection', function (socket) {

        /******************************************** Method for groups  ********************************************************************************
         *
         * **********************************************************************************************************************************************
         */

        /**
         * method to check if  member of group  is start typing
         */
        socket.on('socket_member_typing', function (data) {
            io.sockets.emit('socket_member_typing', {
                recipientId: data.recipientId,
                groupId: data.groupId,
                senderId: data.senderId
            });
        });

        /**
         * method to check if a member of group  is stop typing
         */
        socket.on('socket_member_stop_typing', function (data) {
            io.sockets.emit('socket_member_stop_typing', {
                recipientId: data.recipientId,
                groupId: data.groupId,
                senderId: data.senderId
            });
        });
        /**
         * method to check if u receive a new message
         */
        socket.on('socket_new_group_message', function (data) {
            io.sockets.emit('socket_new_group_message_server', {
                recipientId: data.recipientId,
                messageId: data.messageId,
                messageBody: data.messageBody,
                senderId: data.senderId,
                phone: data.phone,
                senderName: data.senderName,
                GroupImage: data.GroupImage,
                GroupName: data.GroupName,
                groupID: data.groupID,
                date: data.date,
                isGroup: data.isGroup,
                image: data.image,
                video: data.video,
                audio: data.audio,
                document: data.document,
                thumbnail: data.thumbnail,
                duration: data.duration,
                fileSize: data.fileSize
            });

        });

        /**
         * mehtod to save firstly the message in the database
         */
        socket.on('socket_save_group_message', function (data, callback) {
            var http = require('http');
            var queryString = require("querystring");
            var qs = queryString.stringify(data);
            var qslength = qs.length;
            var options = {
                hostname: hostname,
                port: mainPort,
                path: path + '/Groups/saveMessage',
                method: 'POST',
                type: "json",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': data.userToken,
                    'Content-Length': qslength
                }
            };

            var buffer = "";
            var req = http.request(options, function (res) {
                res.on('data', function (chunk) {
                    buffer += chunk;
                });
                res.on('end', function () {

                    var messageData = {
                        messageId: buffer
                    };
                    if (debugging_mode) {
                        console.log(messageData);
                    }
                    callback(messageData);
                    socket.emit('socket_group_sent', {
                        groupId: data.groupID,
                        senderId: data.senderId
                    });
                });

                res.on('error', function (e) {
                    console.log("Got error: " + e.message);
                });
            });

            req.write(qs);
            req.end();
        });

        /**
         * method to ping and check if member of group is connected
         */
        socket.on('socket_user_ping_group', function (data) {

            var pingedData;

            pingedData = {
                recipientId: data.recipientId,
                messageId: data.messageId,
                messageBody: data.messageBody,
                senderId: data.senderId,
                phone: data.phone,
                senderName: data.senderName,
                GroupImage: data.GroupImage,
                GroupName: data.GroupName,
                groupID: data.groupID,
                date: data.date,
                isGroup: data.isGroup,
                image: data.image,
                video: data.video,
                audio: data.audio,
                document: data.document,
                thumbnail: data.thumbnail,
                duration: data.duration,
                fileSize: data.fileSize,
                pinged: data.pinged,
                pingedId: data.pingedId
            };
            io.sockets.emit('socket_user_pinged_group', pingedData);
        });

        /**
         * method to send message group
         */
        socket.on('socket_send_group_message', function (dataString) {
            if (debugging_mode) {
                console.log(dataString);
            }
            saveMessageGroupToDataBase(dataString);
            return;
        });

        /**
         * method to save message as waiting
         * @param data
         */
        function saveMessageGroupToDataBase(data) {

            var http = require('http');
            var queryString = require("querystring");
            var qs = queryString.stringify(data);
            var qslength = qs.length;
            var options = {
                hostname: hostname,
                port: mainPort,
                path: path + '/Groups/send',
                method: 'POST',
                type: "json",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': data.userToken,
                    'Content-Length': qslength
                }
            };

            var buffer = "";
            var req = http.request(options, function (res) {
                res.on('data', function (chunk) {
                    buffer += chunk;
                });
                res.on('end', function () {
                    socket.emit('socket_group_delivered', {
                        groupId: data.groupID,
                        senderId: data.senderId

                    });
                });

                res.on('error', function (e) {
                    console.log("Got error: " + e.message);
                });
            });

            req.write(qs);
            req.end();


        }

        /**
         * method to check if there is messages to sent
         * @param data
         */
        function CheckForUnsentMessages(data) {

            var http = require('http');
            var queryString = require("querystring");
            var qs = queryString.stringify(data);
            var qslength = qs.length;

            var options = {
                hostname: hostname,
                port: mainPort,
                path: path + '/Groups/checkUnsentMessageGroup',
                method: 'POST',
                type: "json",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': data.userToken,
                    'Content-Length': qslength
                }
            };

            var body = "";
            var req = http.request(options, function (res) {
                res.on('data', function (chunk) {
                    body += chunk;
                });
                res.on('end', function () {
                    var obj = JSON.parse(body);
                    for (var i = 0; i < obj.length; i++) {
                        var pingedData = {
                            recipientId: obj[i].recipientId,
                            messageId: obj[i].messageId,
                            messageBody: obj[i].messageBody,
                            senderId: obj[i].senderId,
                            phone: obj[i].phone,
                            senderName: obj[i].senderName,
                            GroupImage: obj[i].GroupImage,
                            GroupName: obj[i].GroupName,
                            groupID: obj[i].groupId,
                            date: obj[i].date,
                            isGroup: obj[i].isGroup,
                            image: obj[i].image,
                            video: obj[i].video,
                            audio: obj[i].audio,
                            document: obj[i].document,
                            thumbnail: obj[i].thumbnail,
                            pinged: obj[i].pinged,
                            pingedId: obj[i].pingedId,
                            duration: obj[i].duration,
                            fileSize: obj[i].fileSize
                        };
                        io.sockets.emit('socket_user_pinged_group', pingedData);
                    }
                });
                res.on('error', function (e) {
                    console.log("Got error: " + e.message);
                });
            });

            req.write(qs);
            req.end();


        }


        /******************************************** Method for a single user ********************************************************************************
         *
         * ****************************************************************************************************************************************************
         */


        /**
         * method to check if user is connected or not
         */

        socket.on('socket_user_connect', function (data) {
            if (debugging_mode) {
                console.log("the user with id " + data.connectedId + " connected " + +data.connected + " token " + data.userToken);
            }
            if (data.connectedId != 0) {
                var user = users.getUser(data.connectedId);
                if (user != null) {
                    users.updateUser(data.connectedId, data.connected, socket.id);
                } else {
                    users.addUser(data.connectedId, data.connected, socket.id);
                }
            }
            io.sockets.emit('socket_user_connect', {
                connectedId: data.connectedId,
                connected: data.connected,
                socketId: socket.id
            });
            CheckForUnsentMessages(data);//this just for groups
        });


        /**
         * method to get response from recipient to update status (from waiting to sent )
         */
        socket.on('socket_send_message', function (dataString) {
            var messageID = {
                messageId: dataString.messageId,
                senderId: dataString.senderId
            };
            io.sockets.emit('socket_send_message', {
                messageId: messageID.messageId,
                senderId: messageID.senderId
            });
        });

        /**
         * method to check if user disconnected  before send a message to him (do a ping and get a callback)
         */
        socket.on('socket_user_ping', function (data, callback) {
            var pingingData = {
                pinged: data.pinged,
                pingedId: data.pingedId,
                socketId: data.socketId
            };

            var pingedData;

            if (pingingData.pingedId = data.recipientId && pingingData.pinged == true) {
                pingedData = {
                    messageId: data.messageId,
                    senderImage: data.senderImage,
                    pingedId: data.recipientId,
                    pinged: pingingData.pinged,
                    senderId: data.senderId,
                    recipientId: data.recipientId,
                    senderName: data.senderName,
                    messageBody: data.messageBody,
                    date: data.date,
                    isGroup: data.isGroup,
                    conversationId: data.conversationId,
                    image: data.image,
                    video: data.video,
                    audio: data.audio,
                    document: data.document,
                    thumbnail: data.thumbnail,
                    phone: data.phone,
                    socketId: data.socketId,
                    duration: data.duration,
                    fileSize: data.fileSize
                };
            } else {
                pingedData = {
                    messageId: data.messageId,
                    senderImage: data.senderImage,
                    pingedId: data.senderId,
                    pinged: pingingData.pinged,
                    senderId: data.senderId,
                    recipientId: data.recipientId,
                    senderName: data.senderName,
                    messageBody: data.messageBody,
                    date: data.date,
                    isGroup: data.isGroup,
                    conversationId: data.conversationId,
                    image: data.image,
                    video: data.video,
                    audio: data.audio,
                    document: data.document,
                    thumbnail: data.thumbnail,
                    phone: data.phone,
                    socketId: data.socketId,
                    duration: data.duration,
                    fileSize: data.fileSize
                };
            }
            callback(pingedData);
            //  return;
        });
        /**
         * method to check if u receive a new message
         */
        socket.on('socket_new_message', function (data) {
            if (debugging_mode) {
                console.log("users connected list size is " + users.getUsers().length);
                console.log("new message is " + data.messageBody + " From user with id " + data.senderId + " to user with id " + data.recipientId);
            }

            var user = users.getUser(data.recipientId);
            if (user != null) {
                if (user.id == data.recipientId) {
                    console.log("socket id " + user.socketID);
                    //   socket.to(user.socketID).emit('socket_new_message_server', {
                    io.sockets.emit('socket_new_message_server', {
                        messageId: data.messageId,
                        senderImage: data.senderImage,
                        senderId: data.senderId,
                        recipientId: data.recipientId,
                        senderName: data.senderName,
                        messageBody: data.messageBody,
                        date: data.date,
                        isGroup: data.isGroup,
                        conversationId: data.conversationId,
                        image: data.image,
                        video: data.video,
                        audio: data.audio,
                        document: data.document,
                        thumbnail: data.thumbnail,
                        phone: data.phone,
                        duration: data.duration,
                        fileSize: data.fileSize
                    });
                }
            }


        });

        /**
         * method to save new message to database
         */
        socket.on('socket_save_new_message', function (data) {
            saveMessageToDataBase(data);
        });
        /**
         * method to save message of user
         * @param data
         */
        function saveMessageToDataBase(data) {
            var http = require('http');
            var queryString = require("querystring");
            var qs = queryString.stringify(data);
            var qslength = qs.length;
            var options = {
                hostname: hostname,
                port: mainPort,
                path: path + '/Messages/send',
                method: 'POST',
                type: "json",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'token': data.userToken,
                    'Content-Length': qslength
                }
            };

            var buffer = "";
            var req = http.request(options, function (res) {
                res.on('data', function (chunk) {
                    buffer += chunk;
                });

                res.on('error', function (e) {
                    console.log("Got error: " + e.message);
                });
            });

            req.write(qs);
            req.end();

        }


        /**
         * method to check if user is start typing
         */
        socket.on('socket_typing', function (data) {
            io.sockets.emit('socket_typing', {
                recipientId: data.recipientId,
                senderId: data.senderId
            });
        });

        /**
         * method to check if user is stop typing
         */
        socket.on('socket_stop_typing', function (data) {
            io.sockets.emit('socket_stop_typing', {
                recipientId: data.recipientId,
                senderId: data.senderId
            });
        });

        /**
         * method to check status last seen
         */
        socket.on('socket_last_seen', function (data) {
            io.sockets.emit('socket_last_seen', {
                lastSeen: data.lastSeen,
                senderId: data.senderId,
                recipientId: data.recipientId
            });
        });


        /**
         * method to check if user is read (seen) a specific message
         */
        socket.on('socket_seen', function (data) {
            io.sockets.emit('socket_seen', {
                senderId: data.senderId,
                recipientId: data.recipientId
            });
        });

        /**
         * method to check if a message is delivered to the recipient
         */
        socket.on('socket_delivered', function (data) {
            io.sockets.emit('socket_delivered', {
                messageId: data.messageId,
                senderId: data.senderId
            });
        });

        /**
         * method to check if recipient is Online
         */
        socket.on('socket_is_online', function (data) {
            io.sockets.emit('socket_is_online', {
                senderId: data.senderId,
                connected: data.connected
            });
        });


        /**
         * method if a user is disconnect from sockets
         * and then remove him from array of current users connected
         */
        socket.on('disconnect', function () {
            if (users.getUsers().length != 0) {
                for (var i = 0; i < users.getUsers().length; ++i) {
                    var user = users[i];
                    if (user != null) {
                        if (debugging_mode) {
                            console.log("the user with id  " + user.id + " is disconnect  ");
                        }
                        io.sockets.emit('socket_user_connect', {
                            connectedId: user.id,
                            connected: false,
                            socketId: user.socketID
                        });
                        if (user.socketID == socket.id) {
                            users.removeUser(user.id);
                            break;
                        }
                    }

                }
            }
        });


    });
};