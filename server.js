/**
 * Created by abderrahimelimame on 9/24/16.
 */

var socket = require('socket.io');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = socket.listen(server);
var port = process.env.PORT || 9000;
var users = require('./node_app/users.js')();

/**
 * You can control those variables as you want
 */

var debugging_mode = true;
var app_key_secret = "0bf5e92854a10bd46682893e00e6ba92";//must be the same for the android project key secret
var mainPort = 80 ;//here is wehere you can change the main port mine is 8888 as default of web app it will be 80
var os = require("os");
var hostname = "10.0.244.129";//this is the ip address copy this and paste it on the project android
var path = "/WhatsCloneBackend";//this is the path where you put all backend files so copy it

/**
 * server listener
 */
server.listen(port, function () {
    console.log('Server listening at port %d', port);
});


/**
 * this for check if the user connect from the app
 */
io.use(function (socket, next) {
    var token = socket.handshake.query.token;
    if (token === app_key_secret) {
        if (debugging_mode) {
            console.log("token valid  authorized", token);
        }
        next();
    } else {
        if (debugging_mode) {
            console.log("not a valid token Unauthorized to access");
        }
        next(new Error("not valid token"));
    }
});

/**
 * Socket.io event handling
 */
require('./node_app/socketHandler.js')(io, users, debugging_mode, path, hostname, os, mainPort);
